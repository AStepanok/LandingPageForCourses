﻿var currentTeacherID = 1;
var isRunning;
var size;

$(document).ready(function () {
    isRunning = true;
    $('.oneTeacher#teacher1').fadeIn(300, function () {
        isRunning = false;
    });
    $('.listOfTeachers > img#teacherIcon1').addClass("activeTeacher");
    size = $('.teachers > .col-md-12 > .oneTeacher*').size();
});

function prev() {
    if (!isRunning) {
        isRunning = true;
        $('.oneTeacher#teacher' + currentTeacherID).fadeOut(300, function () {
            $('.listOfTeachers > img#teacherIcon' + currentTeacherID).removeClass("activeTeacher");
            currentTeacherID--;
            if (currentTeacherID === 0)
                currentTeacherID = size;

            $('.oneTeacher#teacher' + currentTeacherID).fadeIn(300, function () {
                isRunning = false;
            });
            $('.listOfTeachers > img#teacherIcon' + currentTeacherID).addClass("activeTeacher");
        });
    }
};

function next() {
    if (!isRunning) {
        isRunning = true;
        $('.oneTeacher#teacher' + currentTeacherID).fadeOut(300, function () {
            $('.listOfTeachers > img#teacherIcon' + currentTeacherID).removeClass("activeTeacher");
            currentTeacherID++;
            if (currentTeacherID > size)
                currentTeacherID = 1;

            $('.oneTeacher#teacher' + currentTeacherID).fadeIn(300, function () {
                isRunning = false;
            });
            $('.listOfTeachers > img#teacherIcon' + currentTeacherID).addClass("activeTeacher");
        });
    }
};

function goToTeacher(sender) {
    if (!isRunning) {
        isRunning = true;
        if ($(sender).attr("id").substring($(sender).attr("id").length - 1) !== currentTeacherID) {
            $('.oneTeacher#teacher' + currentTeacherID).fadeOut(300, function () {
                $('.listOfTeachers > img#teacherIcon' + currentTeacherID).removeClass("activeTeacher");
                currentTeacherID = $(sender).attr("id").substring($(sender).attr("id").length - 1);


                $('.oneTeacher#teacher' + currentTeacherID).fadeIn(300, function () {
                    isRunning = false;
                });
                $('.listOfTeachers > img#teacherIcon' + currentTeacherID).addClass("activeTeacher");
            });
        }
    }
}