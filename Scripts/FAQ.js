﻿function showAnswer(sender) {
    if (!$(sender).find(".answer").is(':visible'))
        $(sender).find(".answer").slideDown(300);
    else
        $(sender).find(".answer").slideUp(300);
    
};